Initial capacity = 10000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 1 elements = 10000000 random range = 2
Executing time: 48.491 seconds
Total elements removed: 100000

 Sub sizes = [982979, 988458, 994502, 990881, 988728, 987373, 989273, 990883, 993317, 993606]
-----------------------------------------------------
Initial capacity = 20000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 1 elements = 20000000 random range = 2
Executing time: 105.047 seconds
Total elements removed: 100000

 Sub sizes = [1995887, 1991868, 1987214, 1985149, 1995606, 1995685, 1986623, 1981713, 1988085, 1992170]
-----------------------------------------------------
Initial capacity = 30000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 1 elements = 30000000 random range = 2
Executing time: 159.304 seconds
Total elements removed: 100000

 Sub sizes = [2992451, 2990170, 2986765, 2996101, 2991942, 2987764, 2992886, 2989321, 2994277, 2978323]
-----------------------------------------------------
Initial capacity = 40000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 1 elements = 40000000 random range = 2
Executing time: 212.27 seconds
Total elements removed: 100000

 Sub sizes = [4005918, 4011465, 4014486, 4012684, 4015479, 4023824, 3998102, 3988184, 3983026, 3846832]
-----------------------------------------------------
Initial capacity = 50000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 1 elements = 50000000 random range = 2
Executing time: 283.207 seconds
Total elements removed: 100000

 Sub sizes = [4981977, 4985161, 4995205, 4996033, 4992989, 4989540, 4986176, 4986664, 4994353, 4991902]
-----------------------------------------------------
Initial capacity = 10000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 7 elements = 10000000 random range = 2
Executing time: 49.474 seconds
Total elements removed: 100000

 Sub sizes = [990342, 985202, 986317, 994034, 992951, 991365, 989312, 987821, 986233, 996423]
-----------------------------------------------------
Initial capacity = 20000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 7 elements = 20000000 random range = 2
Executing time: 104.952 seconds
Total elements removed: 100000

 Sub sizes = [1987989, 1990392, 1992026, 1994524, 1989941, 1989316, 1990421, 1984949, 1986070, 1994372]
-----------------------------------------------------
Initial capacity = 30000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 7 elements = 30000000 random range = 2
Executing time: 145.279 seconds
Total elements removed: 100000

 Sub sizes = [2999992, 2995651, 2997998, 2964924, 3027302, 2995280, 2941146, 2986277, 3001548, 2989882]
-----------------------------------------------------
Initial capacity = 40000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 7 elements = 40000000 random range = 2
Executing time: 207.518 seconds
Total elements removed: 100000

 Sub sizes = [4047749, 4046887, 4053393, 4044943, 4046295, 4051320, 3925135, 3924001, 3927956, 3832321]
-----------------------------------------------------
Initial capacity = 50000000 grainNum = 10 attemptsToFail = 200
Configuration: threads = 7 elements = 50000000 random range = 2
Executing time: 266.932 seconds
Total elements removed: 100000

 Sub sizes = [4979911, 4992057, 4993259, 4993627, 4989198, 4989773, 4991948, 4993256, 4987048, 4989923]
-----------------------------------------------------
