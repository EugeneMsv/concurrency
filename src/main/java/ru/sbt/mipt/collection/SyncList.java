package ru.sbt.mipt.collection;

import java.util.List;


public interface SyncList<T> {
    public boolean add(T e);

    public boolean remove(T e);

    public boolean contains(T e);

    public T get(int index);

    public int indexOf(T e);

    public T set(int index, T e);

    public int size();

    public boolean isEmpty();

    public void clear();
}
