package ru.sbt.mipt.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Jenius on 28.11.2015.
 * Fine-Grained Synchronization
 */
public class FGSyncArrayList<T> implements SyncList<T> {
    // индексы подлистов для работы remove, contains
    private final LinkedList<Integer> unCheckedIndexes = new LinkedList<>();
    // 2 параметра для add, remove и contains соответственно
    // для add возврат false (можно сделать и задержку) после n (по умолчанию: tuneParameters[0] = 3*subArrayLists) неудачных попыток залочить
    // для remove и contains  задержка (можно сделать и false) после n(по умолчанию: tuneParameters[0] = 3*subArrayLists) неудачных попыток залочить
    //  по умолчанию: tuneParameters[1] = 0 мс
    // значения по умолчанию только в двух конструкторах
    private final int[] tuneParameters; // сейчас не используется
    //подлисты
    private final List<SubArrayList<T>> subArrayLists = new ArrayList<>();
    private final int grains;

    public FGSyncArrayList(Lock[] subLocks,  int capacity, int[] tuneParameters) {
        this.grains = subLocks.length;
        int subCapacity = capacity / subLocks.length;
        subCapacity = subCapacity + 5 * subCapacity / 100; // 5 процента отклонения, для избежания расширения массива;
        for (int i = 0; i < subLocks.length; i++) {
            subArrayLists.add(new SubArrayList<>(subCapacity, subLocks[i]));
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = tuneParameters;
    }

    public FGSyncArrayList(Lock[] subLocks, int[] tuneParameters) {
        this.grains = subLocks.length;
        for (int i = 0; i < subLocks.length; i++) {
            subArrayLists.add(new SubArrayList<>(subLocks[i]));
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = tuneParameters;
    }

    public FGSyncArrayList(int grains, int[] tuneParameters) {
        this.grains = grains;
        for (int i = 0; i < grains; i++) {
            subArrayLists.add(new SubArrayList<>());
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = tuneParameters;
    }

    public FGSyncArrayList(int grains) {
        this.grains = grains;
        for (int i = 0; i < grains; i++) {
            subArrayLists.add(new SubArrayList<>());
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = new int[]{3 * grains, 0};
    }

    public FGSyncArrayList(int grains, int capacity, int[] tuneParameters) {
        this.grains = grains;
        int subCapacity = capacity / grains;
        subCapacity = subCapacity + 5 * subCapacity / 100; // 5 процента отклонения, для избежания расширения массива;
        for (int i = 0; i < grains; i++) {
            subArrayLists.add(new SubArrayList<>(subCapacity));
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = tuneParameters;
    }

    public FGSyncArrayList(int grains, int capacity) {
        this.grains = grains;
        int subCapacity = capacity / grains;
        subCapacity = subCapacity + 5 * subCapacity / 100; // 5 процента отклонения, для избежания расширения массива;
        for (int i = 0; i < grains; i++) {
            subArrayLists.add(new SubArrayList<>(subCapacity));
            this.unCheckedIndexes.add(i);
        }
        this.tuneParameters = new int[]{3 * grains, 0};
    }

    @Override
    public boolean add(T e) {

        Random rand = new Random(System.nanoTime());
        int i = rand.nextInt(grains);
//        int attempt = 0;
        while (!subArrayLists.get(i).tryLock()) {
//            attempt++;
            // тюнить параметр
//            if (attempt >= tuneParameters[0]) {
//                return false;
//            }
            i = rand.nextInt(grains);
        }
        SubArrayList<T> chosenSubArrayList = subArrayLists.get(i);
        try {
            if (chosenSubArrayList != null) {
                return chosenSubArrayList.add(e);
            }
            return false;
        } finally {
            if (chosenSubArrayList != null) {
                chosenSubArrayList.unlock();
            }
        }
    }


    @Override
    public boolean remove(T e) {
        LinkedList<Integer> unCheckedIndexes = new LinkedList<>(this.unCheckedIndexes);
//        System.out.println("initial unCheckedIndexes ="+unCheckedIndexes);
        while (!unCheckedIndexes.isEmpty()) {
            if (subRemove(e, unCheckedIndexes)) {
//                System.out.println("when deleted unCheckedIndexes ="+unCheckedIndexes);
                return true;
            }
//            System.out.println("unCheckedIndexes ="+unCheckedIndexes);
        }
        return false;
    }

    // выбирем случайный нелоченный подлист и удаляем в нем
    private boolean subRemove(T e, LinkedList<Integer> unCheckedIndexes) {
        SubArrayList<T> chosenSubArrayList;
        try {
            chosenSubArrayList = getSubArrayList(unCheckedIndexes);
            try {
                if (chosenSubArrayList != null) {
                    return chosenSubArrayList.remove(e);
                }
                return false;
            } finally {
                if (chosenSubArrayList != null) {
                    chosenSubArrayList.unlock();
                }
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
            return false;
        }
    }

    private SubArrayList<T> getSubArrayList(LinkedList<Integer> unCheckedIndexes) throws InterruptedException {
//        int subAttempt = 0;
        Random rand = new Random(System.nanoTime());
        // индекс в листе индексов
        int i = rand.nextInt(unCheckedIndexes.size());
        // индекс в подлистах
        int currIndex = unCheckedIndexes.get(i);

        while (!subArrayLists.get(currIndex).tryLock()) {
//            subAttempt++;
            // тюнить параметр иначе возможна долгая задержка цикле
//            if (subAttempt >= tuneParameters[0]) {
//                Thread.sleep(tuneParameters[1]);
//            }
            i = rand.nextInt(unCheckedIndexes.size());
            currIndex = unCheckedIndexes.get(i);
        }
        SubArrayList<T> chosenSubArrayList = subArrayLists.get(currIndex);
        unCheckedIndexes.remove(i);

        return chosenSubArrayList;
    }

    @Override
    public boolean contains(T e) {
        LinkedList<Integer> unCheckedIndexes = new LinkedList<>(this.unCheckedIndexes);
        while (!unCheckedIndexes.isEmpty()) {
            if (subContains(e, unCheckedIndexes)) {
                return true;
            }
        }
        return false;
    }

    // выбирем случайный незалоченный подлист и ищем в нем
    private boolean subContains(T e, LinkedList<Integer> unCheckedIndexes) {
        SubArrayList<T> chosenSubArrayList;
        try {
            chosenSubArrayList = getSubArrayList(unCheckedIndexes);
            try {
                if (chosenSubArrayList != null) {
                    return chosenSubArrayList.contains(e);
                }
                return false;
            } finally {
                if (chosenSubArrayList != null) {
                    chosenSubArrayList.unlock();
                }
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
            return false;
        }
    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public int indexOf(T e) {
        return 0;
    }

    @Override
    public T set(int index, T e) {
        return null;
    }

    private void lockAll() {
        for (SubArrayList<T> subArrayList : subArrayLists) {
            subArrayList.lock();
        }
    }

    private void unLockAll() {
        for (SubArrayList<T> subArrayList : subArrayLists) {
            subArrayList.unlock();
        }
    }

    @Override
    public int size() {
        lockAll();
        int fullSize = 0;
        try {
            for (SubArrayList<T> subArrayList : subArrayLists) {
                fullSize += subArrayList.size();
            }
        } finally {
            unLockAll();
        }
        return fullSize;

    }

    public int[] eachSize() {
        lockAll();
        try {
            int[] sizes = new int[grains];
            for (int i = 0; i < grains; i++) {
                sizes[i] = subArrayLists.get(i).size();
            }
            return sizes;
        } finally {
            unLockAll();
        }
    }

    @Override
    public boolean isEmpty() {
        lockAll();
        try {
            for (SubArrayList<T> subArrayList : subArrayLists) {
                if (!subArrayList.isEmpty()) {
                    return false;
                }
            }
            return true;
        } finally {
            unLockAll();
        }
    }

    @Override
    public void clear() {
        lockAll();
        try {
            for (SubArrayList<T> subArrayList : subArrayLists) {
                subArrayList.clear();
            }
        } finally {
            unLockAll();
        }
    }

    @Override
    public String toString() {
        lockAll();
        try {
            StringBuffer result = new StringBuffer();
            for (SubArrayList<T> subArrayList : subArrayLists) {
                result.append(subArrayList.toString());
            }
            return String.valueOf(result);
        } finally {
            unLockAll();
        }
    }
}
