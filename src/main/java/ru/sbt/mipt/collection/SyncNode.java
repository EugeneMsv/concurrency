package ru.sbt.mipt.collection;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Azat on 06.01.2016.
 */
public class SyncNode<T> {
    public SyncNode next;
    public Lock lock;
    public T value;

    public SyncNode(T value) {
        this.value = value;
        lock = new ReentrantLock();
    }

    public void lock() {
        lock.lock();
    }

    public void unlock() {
        lock.unlock();
    }


    public boolean tryLock() {
        return lock.tryLock();
    }

    @Override
    public String toString() {
        if(next != null) {
            return "SyncNode{" +
                    "value=" + value +
                    ", next=" + next.value +
                    '}';
        }else{
            return "SyncNode{" +
                    "value=" + value +
                    ", next=END"  +
                    '}';
        }
    }
}
