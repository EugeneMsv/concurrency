package ru.sbt.mipt.collection;

import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Azat on 06.01.2016.
 */
public class OSLList<T> implements SyncList<T> {
    final SyncNode head;
    final SyncNode half;
    final SyncNode tail;
    private final SyncNode[] chapters;
    private AtomicInteger size;


    public OSLList(int n) {

        SyncNode root = new SyncNode(null);
        SyncNode hhalf = new SyncNode(null);
        SyncNode ttail = new SyncNode(null);
//        root.next = ttail;
        root.next = hhalf;
        hhalf.next = ttail;
        head = root;
        half = hhalf;
        tail = ttail;

        int chapterCount = Math.max(n + 1, 2);
        chapters = new SyncNode[chapterCount];
        SyncNode predNode = new SyncNode(null);
        SyncNode curNode = new SyncNode(null);
        for (int i = 0; i < chapterCount; i++) {
            predNode.next = curNode;
            chapters[i] = predNode;
            predNode = curNode;
            curNode = new SyncNode(null);
        }

        size = new AtomicInteger(0);
    }

    @Override
    public boolean add(T e) {
//        System.out.println(this.toString());
        Random rand = new Random(System.nanoTime());
        int i = rand.nextInt(chapters.length - 1);
        return addToSegment(e, chapters[i], chapters[i + 1]);
    }


    private boolean addToSegment(T e, SyncNode start, SyncNode finish) {
//        System.out.println(this.toString());
        if (e == null) {
            throw new IllegalArgumentException("Null parameter");
        }
        SyncNode pred = start;
        SyncNode curr = pred.next;
        while (true) {
            if (pred.tryLock()) {
                if (curr.tryLock()) {
                    if (validate(pred, curr, start, finish)) {
                        SyncNode node = new SyncNode(e);
                        node.next = curr;
                        pred.next = node;

                        size.incrementAndGet();
                        pred.unlock();
                        curr.unlock();
                        return true;
                    } else {
                        pred.unlock();
                        curr.unlock();
                        if (curr == finish) {
                            pred = start;
                            curr = pred.next;
                        } else {
                            pred = curr;
                            curr = curr.next;
                        }
                    }
                } else {
                    pred.unlock();
                    if (curr == finish) {
                        pred = start;
                        curr = pred.next;
                    } else {
                        pred = curr;
                        curr = curr.next;
                    }
                }
            } else {
                if (curr == finish) {
                    pred = start;
                    curr = pred.next;
                } else {
                    pred = curr;
                    curr = curr.next;
                }
            }
        }
    }

    private boolean addToSegment2(T e, SyncNode start, SyncNode finish) {
//        System.out.println(this.toString());
        if (e == null) {
            throw new IllegalArgumentException("Null parameter");
        }
        SyncNode pred = start;
        SyncNode curr = pred.next;
        while (true) {
            pred.lock();
            curr.lock();
            if (validate(pred, curr, start, finish)) {
                SyncNode node = new SyncNode(e);
                node.next = curr;
                pred.next = node;

                size.incrementAndGet();
                pred.unlock();
                curr.unlock();
                return true;
            } else {
                pred.unlock();
                curr.unlock();
                if (curr == finish) {
                    pred = start;
                    curr = pred.next;
                } else {
                    pred = curr;
                    curr = curr.next;
                }
            }
        }
    }

    @Override
    public boolean remove(T e) {
        for (int i = 0; i < chapters.length - 1; i++) {
            boolean res = removeFromSegment2(e, chapters[i], chapters[i + 1]);
            if (res) {
                return true;
            }
        }
        return false;

    }

    private boolean removeFromSegment(T e, SyncNode start, SyncNode finish) {
//        printSegment(start,finish);
        if (start.next == finish) {
            System.out.println("AHTUNG");
            return false;
        }
        while (true) {
            SyncNode pred = start;
            SyncNode curr = pred.next;
            if (curr.value == null) {
                return false;
            }
//            System.out.println(e);
//            System.out.println(curr);
            while (!curr.value.equals(e)) {
                pred = curr;
                curr = curr.next;
//                System.out.println(pred + " -> " +  curr);
                if (curr == finish) {
                    return false;
                }
//                System.out.println(curr);
            }
            pred.lock();
            curr.lock();
            try {
                if (validate(pred, curr)) {
                    if (curr.value == e) {
                        pred.next = curr.next;
//                        System.out.println("DELETED");
                        size.getAndDecrement();
                        return true;
                    } else {
                        return false;
                    }
                } else {
//                    System.out.println("validate error: " + pred + " -> " +  curr);
//                    System.out.println(pred);
//                    System.out.println(curr);
                    return false;
                }
            } finally {
                pred.unlock();
                curr.unlock();
            }
        }
    }

    private boolean removeFromSegment2(T e, SyncNode start, SyncNode finish) {
//        printSegment(start,finish);

        SyncNode pred;
        SyncNode curr;
        if (start.next == finish || start == finish) {
//            System.out.println("AHTUNG");
            return false;
        }
        pred = start;
        curr = pred.next;
        while (true) {
            if (curr == finish) {
//                System.out.println("er 0");
                return false;
            }
//            System.out.println(e);
//            System.out.println(curr);
            while (!curr.value.equals(e)) {
                pred = curr;
                curr = curr.next;
//                System.out.println(pred + " -> " +  curr);
                if (curr == finish) {
//                    System.out.println("er 1");
                    return false;
                }
//                System.out.println(curr);
            }
            if (pred.tryLock()) {
                if (curr.tryLock()) {
                    try {
                        if (validate(pred, curr)) {
                            if (curr.value == e) {
                                pred.next = curr.next;
//                        System.out.println("DELETED");
                                size.getAndDecrement();
                                pred.unlock();
                                curr.unlock();
                                return true;
                            } else {
//                                System.out.println("equals error: " + pred + " -> " + curr);
                                if (curr.next == finish) {
//                                    System.out.println("er 2");
                                    return false;
                                }
                                pred.unlock();
                                curr.unlock();
                                pred = curr;
                                curr = pred.next;
//                                return false;
                            }
                        } else {
//                            System.out.println("validate error: " + pred + " -> " + curr);
                            if (curr.next == finish) {
//                                System.out.println("er 3");
                                return false;
                            }
                            pred.unlock();
                            curr.unlock();
                            pred = curr.next;
                            curr = pred.next;
//                            pred = start;
//                            curr = pred.next;
                            continue;
//                            pred = curr.next;
//                            curr = pred.next;
//                    System.out.println(pred);
//                    System.out.println(curr);
                        }
                    } finally {
//                        pred.unlock();
//                        curr.unlock();
                    }
                } else {
                    pred.unlock();
                    if (curr.next == finish) {
//                        System.out.println("er 4");
                        return false;
                    }
                    pred = curr.next;
                    curr = pred.next;
//                    pred = start;
//                    curr = pred.next;
//                    return removeFromSegment2(e, pred, finish);
                }
            } else {
                if (curr.next == finish) {
//                    System.out.println("er 5");
                    return false;
                }
                pred = curr;
                curr = pred.next;
//                pred = start;
//                curr = pred.next;
//                return removeFromSegment2(e, pred, finish);
            }
        }
    }

    @Override
    public boolean contains(T e) {
        while (true) {
            SyncNode pred = chapters[0];
            SyncNode curr = pred.next;
            while (!curr.value.equals(e)) {
                pred = curr;
                curr = curr.next;
                if (pred == chapters[chapters.length - 1]) {
                    return false;
                }
            }
            try {
                pred.lock();
                curr.lock();
                if (validate(pred, curr)) {
                    return (curr.value == e);
                } else {
                    return false;
                }
            } finally {
                pred.unlock();
                curr.unlock();
            }
        }
    }


    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public int indexOf(Object e) {
        return 0;
    }

    @Override
    public T set(int index, Object e) {
        return null;
    }

    private int getSize(SyncNode start, SyncNode finish) {
//        printSegment(start, finish);
        SyncNode cur = start;
        cur = start.next;
        int ss = 0;
        while (!cur.equals(finish)) {
//            System.out.println(cur);
            ss++;
            cur = cur.next;
        }
//        System.out.println("ss = " + ss);
        return ss;
    }

    @Override
    public int size() {
//        System.out.println(this.toString());
        int totalSize = 0;
        for (int i = 0; i < chapters.length - 1; i++) {
            int curSize = getSize(chapters[i], chapters[i + 1]);
//            System.out.println("["+i+", " + (i+1) + "] - " + curSize );
            totalSize += curSize;
        }
        System.out.println("TOTAL SIZE: " + totalSize);
        return totalSize;

//        SyncNode cur = head;
//        cur = head.next;
//        int leftSize = 0;
//        boolean isRight = false;
//        int rightSize = 0;
//        int ss = 0;
//        while (cur != tail) {
//            if (isRight) {
//                rightSize++;
//            }
//            if (cur == half) {
//                leftSize = ss;
//                cur = cur.next;
//                isRight = true;
//                continue;
//            }
//            ss++;
//            cur = cur.next;
//        }
//        System.out.println("ss = " + ss + " || size = " + size.get());
//        System.out.println("left size: " + leftSize + " right size: " + rightSize);
//        return size.get();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("* -> ");
//        SyncNode start = head;
        SyncNode start = chapters[0];
        start = start.next;
        while (!start.equals(chapters[chapters.length - 1])) {
//            System.out.println(start.toString());
            stringBuffer.append(start.value).append(" -> ");
            start = start.next;
        }
        stringBuffer.append(" *");
        return stringBuffer.toString();
    }

    public void printSegment(SyncNode left, SyncNode right) {
        System.out.println("START PRINT SEGMENT");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("* -> ");
//        SyncNode start = head;
        SyncNode start = left;
        start = start.next;
        while (!start.equals(right)) {
//            System.out.println(start.toString());
            stringBuffer.append(start.value).append(" -> ");
            start = start.next;
        }
        stringBuffer.append(" *");
        System.out.println(stringBuffer.toString());
    }

    private boolean validate(SyncNode pred, SyncNode curr) {
        SyncNode node = chapters[0];
        while (node != chapters[chapters.length - 1]) {
            if (node == pred) {
                return pred.next == curr;
            }
            node = node.next;
        }
        return false;
    }

    private boolean validate(SyncNode pred, SyncNode curr, SyncNode start, SyncNode finish) {
        SyncNode node = start;
        while (node != finish) {
            if (node == pred) {
                return pred.next == curr;
            }
            node = node.next;
        }
        return false;

    }

}
