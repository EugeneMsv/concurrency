package ru.sbt.mipt.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Jenius on 04.01.2016.
 */
public class SubArrayList<T> implements SyncList<T> {


    /**
     * default capacity
     */

    private final int defaultCapacity = 10;
    /**
     * data storage
     */
    private Object[] data;
    /**
     * amount of stored elements in the list
     */
    private int size;

    private Lock lock;

    public SubArrayList(int capacity, Lock lock) {
        if (capacity > 0) {
            this.data = new Object[capacity];
            this.lock = lock;
        } else throw new IllegalArgumentException("capacity must be positive");
    }

    public SubArrayList(int capacity) {
        if (capacity > 0) {
            this.data = new Object[capacity];
            this.lock = new ReentrantLock();
        } else throw new IllegalArgumentException("capacity must be positive");

    }

    public SubArrayList(Lock lock) {
        this.lock = lock;
        this.data = new Object[defaultCapacity];
    }

    public SubArrayList() {
        this.data = new Object[defaultCapacity];
        this.lock = new ReentrantLock();
    }


    public void lock() {
        lock.lock();
    }

    public boolean tryLock() {
        return lock.tryLock();
    }

    public void unlock() {
        lock.unlock();
    }

    @Override
    public int size() {
        return size;
    }


    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T e) {
//        try {
//            Thread.sleep(50);
//        } catch (InterruptedException e1) {
//            e1.printStackTrace();
//        }
        nullCheck(e);
        if (size < data.length) {
            data[size++] = e;
        } else {
            data = expandArray(data);
            this.fastAdd(e);
        }
        return true;
    }

    @Override
    public boolean contains(T e) {
        nullCheck(e);
        for (int i = 0; i < size; i++) {
            if (e.equals(data[i])) {
                return true;
            }
        }
        return false;
    }

    private void fastAdd(T e) {
        data[size++] = e;
    }

    @Override
    public boolean remove(T e) {
        nullCheck(e);
        for (int i = 0; i < size; i++) {
            if (e.equals(data[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    private T remove(int index) {
        rangeCheck(index);
        T removedElement = (T) data[index];
        int movedElementsAmount = size - index - 1;
        //проверить кол-во перемещаемых элементов справа
        if (movedElementsAmount > 0) {
            System.arraycopy(data, index + 1, data, index, movedElementsAmount);
        }
        // очищаем последний
        data[--size] = null;
        return removedElement;
    }

    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        rangeCheck(index);
        return (T) data[index];
    }

    @Override
    public T set(int index, T element) {
        rangeCheck(index);
        T previousValue = (T) data[index];
        data[index] = element;
        return previousValue;
    }

    @Override
    public int indexOf(T e) {
        nullCheck(e);
        for (int i = 0; i < size; i++) {
            if (e.equals(data[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void clear() {
        data = new Object[defaultCapacity];
        size = 0;
    }

    //не оптимально реализован, но метод нужен только для проверок, лучше не вызывать для больших размеров
    @Override
    public String toString() {
        Object[] printedData = new Object[size];
        System.arraycopy(data, 0, printedData, 0, size);
        return "SyncArrayList{" +
                "data=" + Arrays.toString(printedData) +
                '}';
    }

    /**
     * @param previousArray - old array
     * @return new expanded array with all old elements
     * doesn't support case when size greater then Integer.MAX_VALUE (no exceptions)
     */
    private Object[] expandArray(Object[] previousArray) {
        int currentLength = previousArray.length;
        int newLength;

        if (currentLength > Integer.MAX_VALUE / 2) {
            newLength = Integer.MAX_VALUE;
        } else newLength = 2 * currentLength;

        Object[] expandedArray = new Object[newLength];
        System.arraycopy(previousArray, 0, expandedArray, 0, currentLength);
        return expandedArray;
    }


    /**
     * @param index should be in allowed range
     *              if not - exception
     */
    private void rangeCheck(int index) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("illegal index value, index = " + index + " size = " + size);
    }

    private void nullCheck(T o) {
        if (o == null)
            throw new IllegalArgumentException("null is not allowed");
    }

}
