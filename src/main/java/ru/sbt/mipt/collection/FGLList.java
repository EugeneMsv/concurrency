package ru.sbt.mipt.collection;

import ru.sbt.mipt.synchronization.BackoffLock;

import java.util.concurrent.locks.Lock;

/**
 * Created by Azat on 04.01.2016.
 */
public class FGLList<T> implements SyncList<T> {
    private FGNode head;
    private FGNode tail;
    private int size;

    public FGLList() {
        FGNode root = new FGNode(null);
        head = root;
        tail = root;
        size = 0;
    }

    @Override
    public boolean add(T e) {
        while(true){
            if(tail.tryLock()){
                break;
            }
        }
        FGNode pred = tail;
        try {
            FGNode newNode = new FGNode(e);
            pred.next = newNode;
            tail = newNode;
            size++;
            return true;
        } finally {
            pred.unlock();
        }
    }

    @Override
    public boolean remove(T e) {
        FGNode pred = null, curr = null;
        head.lock();
        try {
            pred = head;
            curr = pred.next;
            curr.lock();
            try {
                while (!curr.value.equals(e)) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    curr.lock();
                }
                if (curr.value.equals(e)) {
                    pred.next = curr.next;
                    size--;
                    return true;
                }
                return false;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public boolean contains(T e) {
        FGNode pred = null, curr = null;
        head.lock.lock();
        try {
            pred = head;
            curr = pred.next;
            curr.lock();
            try {
                while (!curr.value.equals(e)) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    curr.lock();
                }
                if (curr.value.equals(e)) {
                    return true;
                }
                return false;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public T get(int index) {
        if (index >= size) {
            return null;
        }
        FGNode pred = null, curr = null;
        head.lock();
        try {
            pred = head;
            curr = pred.next;
            curr.lock();
            int i = 0;
            try {
                while (i < index) {
                    pred.unlock();
                    pred = curr;
                    curr = curr.next;
                    i++;
                    curr.lock();
                }
                return (T) curr.value;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public int indexOf(T e) {
        FGNode pred = null, curr = null;
        head.lock();
        try {
            pred = head;
            curr = pred.next;
            int index = 0;
            curr.lock();
            try {
                while (!curr.value.equals(e)) {
                    pred.lock.unlock();
                    pred = curr;
                    curr = curr.next;
                    index++;
                    curr.lock();
                }
                if (curr.value.equals(e)) {
                    return index;
                }
                return -1;
            } finally {
                curr.unlock();
            }
        } finally {
            pred.unlock();
        }
    }

    @Override
    public T set(int index, T e) {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        FGNode root = new FGNode(null);
        head = root;
        tail = root;
        size = 0;
    }

    private class FGNode<T> {
        private FGNode next;
        private Lock lock;
        private T value;

        public FGNode(T value) {
            this.value = value;
            lock = new BackoffLock();
        }

        private void lock() {
            lock.lock();
        }

        private void unlock() {
            lock.unlock();
        }

        public boolean tryLock() {
            return lock.tryLock();
        }
    }
}
