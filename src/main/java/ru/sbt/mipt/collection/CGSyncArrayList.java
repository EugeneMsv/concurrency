package ru.sbt.mipt.collection;

import ru.sbt.mipt.synchronization.TASLock;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Jenius on 18.11.2015.
 * Coarse-Grained Synchronization
 */
public class CGSyncArrayList<T> implements SyncList<T> {

    /**
     * default capacity
     */

    private final int defaultCapacity = 10;
    /**
     * data storage
     */
    private Object[] data;
    /**
     * amount of stored elements in the list
     */
    private int size;

    /**
     * single lock
     */
    private Lock lock;


    public CGSyncArrayList(int capacity, Lock chosenLock) {
        if (capacity > 0) {
            this.data = new Object[capacity];
        } else throw new IllegalArgumentException("capacity must be positive");
        this.lock = chosenLock;
    }

    public CGSyncArrayList(Lock chosenLock) {
        this.data = new Object[defaultCapacity];
        this.lock = chosenLock;
    }

    public CGSyncArrayList(int capacity) {
        if (capacity > 0) {
            this.data = new Object[capacity];
        } else throw new IllegalArgumentException("capacity must be positive");
        this.lock = new ReentrantLock();
    }

    public CGSyncArrayList() {
        this.data = new Object[defaultCapacity];
        this.lock = new ReentrantLock();
    }

    @Override
    public int size() {
        lock.lock();
        try {
            return size;
        } finally {
            lock.unlock();
        }
    }


    @Override
    public boolean isEmpty() {
        lock.lock();
        try {
            return size == 0;
        } finally {
            lock.unlock();
        }
    }

    //по факту добавление всегда происходит последовательно, при попытке паралелльно - лочится
    @Override
    public boolean add(T e) {
        nullCheck(e);
        lock.lock();
        try {

            if (size < data.length) {
                data[size++] = e;
            } else {
                data = expandArray(data);
                this.fastAdd(e);
            }
            return true;
        } finally {
            lock.unlock();
        }

    }

    @Override
    public boolean contains(T e) {
        nullCheck(e);
        lock.lock();
        try {
            for (int i = 0; i < size; i++) {
                if (e.equals(data[i])) {
                    return true;
                }
            }
            return false;
        } finally {
            lock.unlock();
        }
    }

    // не вызывать вне критической секции т.к. не Защищен локом
    private void fastAdd(T e) {
        data[size++] = e;
    }

    @Override
    public boolean remove(T e) {
        nullCheck(e);
        lock.lock();
        try {
            for (int i = 0; i < size; i++) {
                if (e.equals(data[i])) {
                    remove(i);
                    return true;
                }
            }
            return false;
        } finally {
            lock.unlock();
        }

    }

    // не вызывать вне критической секции т.к. не Защищен локом
    private T remove(int index) {
        rangeCheck(index);
        T removedElement = (T) data[index];
        int movedElementsAmount = size - index - 1;

        //проверить кол-во перемещаемых элементов справа
        if (movedElementsAmount > 0) {
            System.arraycopy(data, index + 1, data, index, movedElementsAmount);
        }
        // очищаем последний
        data[--size] = null;

        return removedElement;

    }

    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        lock.lock();
        try {
            rangeCheck(index);
            return (T) data[index];

        } finally {
            lock.unlock();
        }

    }

    @Override
    public T set(int index, T element) {
        lock.lock();
        try {
            rangeCheck(index);
            T previousValue = (T) data[index];
            data[index] = element;
            return previousValue;
        } finally {
            lock.unlock();
        }

    }

    @Override
    public int indexOf(T e) {
        nullCheck(e);
        lock.lock();
        try {
            for (int i = 0; i < size; i++) {
                if (e.equals(data[i])) {
                    return i;
                }
            }
            return -1;
        } finally {
            lock.unlock();
        }

    }

    @Override
    public void clear() {
        lock.lock();
        try {
            data = new Object[defaultCapacity];
            size = 0;
        } finally {
            lock.unlock();
        }

    }

    //не оптимально реализован, но метод нужен только для проверок, лучше не вызывать для больших размеров
    @Override
    public String toString() {
        Object[] printedData = new Object[size];
        System.arraycopy(data, 0, printedData, 0, size);
        return "SyncArrayList{" +
                "data=" + Arrays.toString(printedData) +
                '}';
    }

    /**
     * @param previousArray - old array
     * @return new expanded array with all old elements
     * doesn't support case when size greater then Integer.MAX_VALUE (no exceptions)
     */
    private Object[] expandArray(Object[] previousArray) {
        int currentLength = previousArray.length;
        int newLength;

        if (currentLength > Integer.MAX_VALUE / 2) {
            newLength = Integer.MAX_VALUE;
        } else newLength = 2 * currentLength;

        Object[] expandedArray = new Object[newLength];
        System.arraycopy(previousArray, 0, expandedArray, 0, currentLength);
        return expandedArray;
    }

    /**
     * @param index should be in allowed range
     *              if not - exception
     */
    //не вызывать вне критической секции
    private void rangeCheck(int index) {
        if (index >= size || index < 0)
            throw new IndexOutOfBoundsException("illegal index value, index = " + index + " size = " + size);
    }

    private void nullCheck(T o) {
        if (o == null)
            throw new IllegalArgumentException("null is not allowed");
    }

}
