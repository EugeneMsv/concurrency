package ru.sbt.mipt.synchronization;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Created by Azat on 02.01.2016.
 */
public class BackoffLock implements Lock {
    private static final int MIN_DELAY = 10;
    private static final int MAX_DELAY = 1000;
    private AtomicBoolean state = new AtomicBoolean(false);

    @Override
    public void lock() {
        BackOff backOff = new BackOff(MIN_DELAY, MAX_DELAY);
        while (true) {
            while (state.get()) {
            }

            if (!state.getAndSet(true)) {
                return;
            } else {
                try {
                    backOff.backOff();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void unlock() {
        state.set(false);
    }


    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock(){
        if (!state.get()){
            lock();
            return true;
        } else return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public Condition newCondition() {
        return null;
    }


    public class BackOff {
        final int minDelay, maxDelay;
        final Random random;
        int limit;

        public BackOff(int min, int max) {
            minDelay = min;
            maxDelay = min;
            limit = minDelay;
            random = new Random();
        }

        public void backOff() throws InterruptedException {
            int delay = random.nextInt(limit);
            limit = Math.min(maxDelay, 2 * limit);
            Thread.sleep(delay);
        }
    }
}
