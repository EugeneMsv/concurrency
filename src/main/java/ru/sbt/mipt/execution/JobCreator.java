package ru.sbt.mipt.execution;

import org.junit.runners.model.InitializationError;
import ru.sbt.mipt.collection.SyncList;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jenius on 04.01.2016.
 */
//класс для использования в тестах
public class JobCreator {

    public static List<Thread> createAddThreads(final SyncList list, int numberOfThreads, int numberOfElements, final int randomRange) throws IOException {

        List<Thread> threads = new ArrayList<Thread>();
        final int threadElemAmount = numberOfElements / numberOfThreads;
        int rest = numberOfElements % numberOfThreads;
        for (int i = 0; i < numberOfThreads; i++) {
            int additive = 0;
            if (rest > 0) {
                additive = 1;
            }
            final int finalAdditive = additive;
            threads.add(new ExecThread(new Runnable() {
                @Override
                public void run() {
                    Random rand = new Random(System.nanoTime());
                    for (int j = 0; j < (threadElemAmount + finalAdditive); j++) {
//                        list.add(randomRange + rand.nextInt(randomRange));
                        list.add(rand.nextInt(randomRange));
//                        list.add(j);
                    }

                }
            }, String.valueOf("adding " + i)));
            rest--;
        }
        return threads;
    }


    /**
     * Создаем несколько потоков и каждый должен
     * добавить одинаковое(почти) кол-во элементов
     *
     * @param list
     * @param numberOfThreads
     * @param bw               for file output
     * @param numberOfElements - elements in all List, needed for creation
     * @throws IOException
     * @throws InterruptedException
     */
    public static void addTest(final SyncList list, int numberOfThreads, BufferedWriter bw, int numberOfElements, final int randomRange) throws IOException, InterruptedException {

        List<Thread> threads = createAddThreads(list, numberOfThreads, numberOfElements, randomRange);
        bw.write("Configuration: threads = " + numberOfThreads + " elements = " + numberOfElements + " random Range = " + randomRange);
        bw.newLine();
        System.out.println("Start adding test with " + numberOfThreads + " threads");
        long start = System.currentTimeMillis();
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        long result = System.currentTimeMillis() - start;
        bw.write("Executing time: " + (double) result / 1000.0 + " seconds");
        bw.newLine();
        bw.write("Total elements added: " + list.size());
        bw.newLine();
        bw.newLine();
//        bw.write("--------------------------------------------------------");
//        bw.newLine();
        System.out.println("add test done");
        System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
        System.out.println("----------------------------------------------");
        assertEquals(numberOfElements, list.size());
    }

    /**
     * *Создаем несколько потоков и каждый должен
     * удалить одинаковое(почти) кол-во элементов
     *
     * @param list
     * @param numberOfThreads
     * @param bw               - for file output
     * @param numberOfElements - elements in all List, needed for creation
     * @param listIsEmpty      - when true run fillList
     * @throws InterruptedException
     * @throws IOException
     */
    public static void removeZerosTest(final SyncList list, int numberOfThreads, BufferedWriter bw, int numberOfElements, int toRemove, int randomRange, boolean listIsEmpty) throws InterruptedException, IOException, InitializationError {
        //заполнение в один поток если лист пуст
        int zeros = 0;
        if (listIsEmpty) {
            zeros = fillList(list, numberOfElements, randomRange);
        }
        if (zeros < toRemove) {
            throw new InitializationError("you want delete too much elements, list doesn't contains all of them");
        }
        List<Thread> threads = createRemoveThreads(list, numberOfThreads, toRemove);

        bw.write("Configuration: threads = " + numberOfThreads + " elements = " + numberOfElements + " random range = " + randomRange);
        bw.newLine();
        long start = System.currentTimeMillis();
        System.out.println("Start removing test with " + numberOfThreads + " threads");

        threads.forEach(Thread::start);

        for (Thread thread : threads) {
            thread.join();
        }
        long result = System.currentTimeMillis() - start;

        bw.write("Executing time: " + (double) result / 1000.0 + " seconds");
        bw.newLine();
        bw.write("Total elements removed: " + (numberOfElements - list.size()));
        bw.newLine();
        bw.newLine();
//        bw.write("--------------------------------------------------------");
//        bw.newLine();
        System.out.println("removed test done");
        System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
        System.out.println("----------------------------------------------");
        assertEquals(toRemove, (numberOfElements - list.size()));
    }

    /**
     * @param list
     * @param numberOfThreads
     * @param removeElements  - amount of removed elements
     * @return
     */
    // текущая реализация создает потоки для удаления (Integer) 0
    public static List<Thread> createRemoveThreads(final SyncList list, int numberOfThreads, int removeElements) {
        List<Thread> threads = new ArrayList<Thread>();
        // т.к. делиться может с остатком, то распределяем остатки
        final int threadElemAmount = removeElements / numberOfThreads;
        int rest = removeElements % numberOfThreads;
//        System.out.println("rest " + rest);

        for (int i = 0; i < numberOfThreads; i++) {
            int additive = 0;
            if (rest > 0) {
                additive = 1;
            }
            final int finalAdditive = additive;
            threads.add(new ExecThread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < (threadElemAmount + finalAdditive); j++) {
//                        System.out.println("remove step = "+j);
                        list.remove((Integer) 0);
                    }
                }
            }, String.valueOf("removing " + i)));
//            System.out.println("threadElemAmount  " + threadElemAmount + " finalAdditive " + finalAdditive);
            rest--;
        }
        return threads;
    }

    //наполнение случайными числами одним потоком

    /**
     * @param list
     * @param numberOfElements
     * @return amount of zeros in list needed for remove zeros and compare amounts
     * @throws InterruptedException
     */
    public static int fillList(final SyncList list, int numberOfElements, int randomRange) throws InterruptedException {
        Random rand = new Random(System.nanoTime());
        int zeros = 0;
        for (int j = 0; j < numberOfElements; j++) {
            int currentInt = rand.nextInt(randomRange);
            if (currentInt == 0) {
                zeros++;
            }
            list.add(currentInt);
        }
        return zeros;
    }

    /* ПЕРЕД ИСПОЛЬЗОВАНИЕМ в методе createAddThreads заменить
     list.add(randomRange+rand.nextInt(randomRange));
     на list.add(rand.nextInt(randomRange)); ОБЯЗАТЕЛЬНО, ИНАЧЕ НЕ БУДЕТ УДАЛЯТЬ
    */
    public static void mixedTest(SyncList list, int addThreadsAmount, int rmvThreadsAmount, BufferedWriter bw, int addElements, int removeElements, int randomRange, int removeDelay) throws IOException, InterruptedException {
        // параметра рандом нет т.к. добавляем только элементы
        List<Thread> addThreads = JobCreator.createAddThreads(list, addThreadsAmount, addElements, randomRange);
        List<Thread> removeThreads = JobCreator.createRemoveThreads(list, rmvThreadsAmount, removeElements);
        bw.write("Configuration: add Threads = "+addThreadsAmount+" remove Threads = "+rmvThreadsAmount);
        bw.newLine();
        bw.write("add elements = "+ addElements+" remove elements " + removeElements);
        bw.newLine();
        bw.write("random range = "+ randomRange + " remove delay = "+ removeDelay);
        bw.newLine();
        System.out.println(" mixed test started");
        long start = System.currentTimeMillis();
        addThreads.forEach(java.lang.Thread::start);
        Thread.sleep(removeDelay);
        removeThreads.forEach(Thread::start);

        for (Thread addThread : addThreads) {
            addThread.join();
        }

        for (Thread removeThread : removeThreads) {
            removeThread.join();
        }

        long result = System.currentTimeMillis() - start;
        bw.write("Executing time: " + (double) result / 1000.0 + " seconds");
        bw.newLine();
        System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
        assertEquals(addElements - removeElements, list.size());
    }

}
