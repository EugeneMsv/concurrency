package ru.sbt.mipt.execution;

/**
 * Created by Jenius on 24.11.2015.
 */
public class ExecThread extends Thread {
    String id;

    public ExecThread(Runnable target, String id) {
        super(target);
        this.id = id;
    }

    public String getMyId() {
        return id;
    }

    @Override
    public void run() {
        System.out.println("Thread " + id + " started");
        super.run();
        System.out.println("Thread " + id + " ended");
    }
}
