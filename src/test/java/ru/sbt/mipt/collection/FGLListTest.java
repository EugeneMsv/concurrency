package ru.sbt.mipt.collection;

import org.junit.Test;

import static org.junit.Assert.*;

public class FGLListTest {

    @Test
    public void testAdd() throws Exception {
        FGLList<Integer> integers = new FGLList<>();
        for (int i = 0; i < 5; i++) {
            integers.add(i);
        }
        System.out.println("addTest");
        System.out.println(integers.toString());
        assertEquals(5,integers.size());

    }

    @Test
    public void testRemove() throws Exception {
        SyncList<Integer> integers = new FGLList<Integer>();
        for (int i = 0; i < 5; i++) {
            integers.add(i*7);
        }
        System.out.println("\nremoveFirstObjTest");
        System.out.println(integers.toString());
        integers.remove((Integer) 21);
        System.out.println(integers.toString());
        assertEquals(4, integers.size());
    }

    @Test
    public void testContains() throws Exception {
        SyncList<Integer> integers = new FGLList<Integer>();
        for (int i = 0; i < 5; i++) {
            integers.add(i*7);
        }
        System.out.println("\nremoveFirstObjTest");
        System.out.println(integers.toString());
        assertTrue(integers.contains((Integer) 21));
    }


    @Test
    public void testIndexOf() throws Exception {

    }

    @Test
    public void testSet() throws Exception {

    }

    @Test
    public void testSize() throws Exception {

    }

    @Test
    public void testIsEmpty() throws Exception {

    }

    @Test
    public void testClear() throws Exception {

    }
}