package ru.sbt.mipt.collection;

import org.junit.Test;
import ru.sbt.mipt.execution.JobCreator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ThreadsOSLListTest {

    @Test
    public void testAdd() throws Exception {
        File outputAdd = new File("files\\OneWayTests\\OSLLAdd-put.txt");
        try (BufferedWriter bwAdd = new BufferedWriter(new FileWriter(outputAdd, true))) {
            int grain = 8;
            OSLList<Integer> integers = new OSLList<>(grain);
            int threads = 1;
            int elements = 40_000_000;
            bwAdd.write("Grain = " + grain);
            bwAdd.newLine();
            JobCreator.addTest(integers, threads, bwAdd, elements, 100_000_000);
            bwAdd.write("----------------------------------------------");
            bwAdd.newLine();
//            System.out.println(integers);
//            System.out.println(integers.toString());
        }
    }

    @Test
    public void testRemove() throws Exception {
        int threads = 4;
        int elements = 4_000_000;
        int toRemove = 6_000;
        int grainNum = 20;//ТЮНИТЬ

        File outputRemoveLast = new File("files\\OneWayTests\\OSLLRemove-put.txt");
        try ( BufferedWriter bwRemove = new BufferedWriter(new FileWriter(outputRemoveLast, true))) {
            OSLList<Integer> integers = new OSLList<>(grainNum);
            bwRemove.write("Initial capacity = " + elements + " grainNum = " + grainNum);
            bwRemove.newLine();
            JobCreator.removeZerosTest(integers, threads, bwRemove, elements, toRemove, 2, true);
            bwRemove.write("-----------------------------------------------------");
            bwRemove.newLine();
            System.out.println("Size = " + integers.size());
        }
    }

    @Test
    public void testMixed() throws Exception {
        File directory = new File("files\\mixedTests");
        directory.mkdirs();

        File output = new File("files\\mixedTests\\OSAddWithRemove.txt");
        //в данном тесте одниковое кол-во потоков удаляет и добавляет
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(output, true))) {
            int grain = 20;
            int addThreads = 1;
            int removeThreads = 6;
            int addElements = 20_000_000;
            int removeElements = 15_000;
            int randomRange = 100;
            int removeDelay = 5000;
            SyncList<Integer> integers = new OSLList<>(grain);

            bw.write("Initial grain = " + grain);
            bw.newLine();
            JobCreator.mixedTest(integers, addThreads, removeThreads, bw, addElements, removeElements, randomRange, removeDelay);
            bw.write("------------------------------------------");
            bw.newLine();

        }
    }
}