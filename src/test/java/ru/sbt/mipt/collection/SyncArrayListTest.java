package ru.sbt.mipt.collection;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jenius on 18.11.2015.
 */
public class SyncArrayListTest {


    @Test
    public void addTest() {
        SyncList<Integer> integers = new CGSyncArrayList<Integer>();
        for (int i = 0; i < 5; i++) {
            integers.add(i);
        }
        System.out.println("addTest");
        System.out.println(integers.toString());
        assertEquals(5,integers.size());
    }


    @Test
    public void addWithExpandTest() {
        SyncList<Integer> integers = new CGSyncArrayList<Integer>();
        for (int i = 0; i < 15; i++) {
            integers.add(i);
        }
        System.out.println("\naddWithExpandTest");
        System.out.println(integers.toString());
        assertEquals(15,integers.size());
    }


    @Test
    public void removeObjTest() {
        SyncList<Integer> integers = new CGSyncArrayList<Integer>();
        for (int i = 0; i < 5; i++) {
            integers.add(i*7);
        }
        System.out.println("\nremoveFirstObjTest");
        System.out.println(integers.toString());
        integers.remove((Integer) 21);
        System.out.println(integers.toString());
        assertEquals(4, integers.size());
    }


    @Test
    public void clearTest() {
        SyncList<Integer> integers = new CGSyncArrayList<Integer>();
        for (int i = 0; i < 8; i++) {
            integers.add(i+1);
        }
        for (int i = 0; i < 8; i++) {
            integers.add(i*10);
        }
        System.out.println("\nclearTest");
        System.out.println(integers.toString());
        integers.clear();
        System.out.println(integers.toString());
        assertEquals(0, integers.size());
    }



}
