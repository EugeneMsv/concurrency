package ru.sbt.mipt.collection;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OSLListTest {

    @Test
    public void testAdd() throws Exception {
        SyncList<Integer> integers = new OSLList<>(2);
        for (int i = 0; i < 5; i++) {
            integers.add(i);
        }
        System.out.println("addTest");
        System.out.println(integers.toString());
        assertEquals(5,integers.size());
    }

    @Test
    public void testRemove() throws Exception {
        SyncList<Integer> integers = new OSLList<Integer>(2);
        for (int i = 0; i < 5; i++) {
            integers.add(i*7);
        }
        System.out.println("\nremoveFirstObjTest");
        System.out.println(integers.toString());
        integers.remove((Integer) 14);
        System.out.println(integers.toString());
        assertEquals(4, integers.size());
    }

    @Test
    public void testContains() throws Exception {
        SyncList<Integer> integers = new OSLList<Integer>(2);
        for (int i = 0; i < 5; i++) {
            integers.add(i*7);
        }
        System.out.println("\nContainObjTest");
        System.out.println(integers.toString());
        assertTrue(integers.contains((Integer) 21));
    }
}