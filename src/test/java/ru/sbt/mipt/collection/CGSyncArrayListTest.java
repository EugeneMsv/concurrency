package ru.sbt.mipt.collection;

import org.junit.Test;
import org.junit.runners.model.InitializationError;
import ru.sbt.mipt.execution.JobCreator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Jenius on 18.11.2015.
 * Test Coarse-Grained Synchronization ArrayList
 */
public class CGSyncArrayListTest {

    /**
     * Однонаправленные тест только на добавление или только на удаление элементов
     *
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void simpleOneWayTestsOutput() throws IOException, InterruptedException, InitializationError {
        File directory = new File("files\\OneWayTests");
        directory.mkdirs();
        //добавление
        int threads = 7; //ТЮНИТЬ
        boolean appendToFile = true;//ТЮНИТЬ
        File outputAdd = new File("files\\OneWayTests\\CGAdd-put.txt");
        //удаление
        File outputRemoveLast = new File("files\\OneWayTests\\CGRemove-put.txt");
        try (BufferedWriter bwAdd = new BufferedWriter(new FileWriter(outputAdd, appendToFile)); BufferedWriter bwRemove = new BufferedWriter(new FileWriter(outputRemoveLast, appendToFile))) {

            //добавление
            int addCapacity = 1;
            SyncList<Integer> integers = new CGSyncArrayList<Integer>(addCapacity);
            bwAdd.write("Initial capacity = " + addCapacity);
            bwAdd.newLine();
            JobCreator.addTest(integers, threads, bwAdd, 85_000_000, 100_000_000);
            bwAdd.write("-------------------------------------------------------");
            bwAdd.newLine();

//            //удаление
            int rmvCapacity = 4_000_000;
            SyncList<Integer> integers2 = new CGSyncArrayList<Integer>(rmvCapacity);
            bwRemove.write("Initial capacity = " + rmvCapacity);
            bwRemove.newLine();
            JobCreator.removeZerosTest(integers2, threads, bwRemove, rmvCapacity, 50_000, 2, true);
            bwRemove.write("-------------------------------------------------------");
            bwRemove.newLine();
        }
    }

    @Test
    public void AddMixRemoveTestsOutput() throws IOException, InterruptedException {
        File directory = new File("files\\mixedTests");
        directory.mkdirs();

        File output = new File("files\\mixedTests\\CGAddWithRemove.txt");
        //в данном тесте одниковое кол-во потоков удаляет и добавляет
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(output,true))) {
            int capacity = 1;
            int addThreads = 1;
            int removeThreads = 6;
            int addElements = 150_000_000;
            int removeElements = 10_000;
            int randomRange = 1000;
            int removeDelay = 10000;
            SyncList<Integer> integers = new CGSyncArrayList<>(capacity);

            bw.write("Initial capacity = " + capacity);
            bw.newLine();
            JobCreator.mixedTest(integers, addThreads, removeThreads, bw, addElements, removeElements, randomRange, removeDelay);
            bw.write("------------------------------------------");
            bw.newLine();

        }
    }


}

