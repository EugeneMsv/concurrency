package ru.sbt.mipt.collection;

import org.junit.Test;
import ru.sbt.mipt.execution.JobCreator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class FGSyncArrayListTest {

    @Test
    public void testSimple() throws Exception {

        File directory = new File("files\\OneWayTests");
        directory.mkdirs();
        //добавление
        int threads = 7; //ТЮНИТЬ
        boolean appendToFile = true;//ТЮНИТЬ
        int grainNum = 10;//ТЮНИТЬ
        int attemptsToFail = 200;//- не актуально

        File outputAdd = new File("files\\OneWayTests\\FGAdd-put.txt");
        //удаление
        File outputRemoveLast = new File("files\\OneWayTests\\FGRemove-put.txt");
        try (
                BufferedWriter bwAdd = new BufferedWriter(new FileWriter(outputAdd, appendToFile));
             BufferedWriter bwRemove = new BufferedWriter(new FileWriter(outputRemoveLast, appendToFile))
        ) {

            //добавление
            int addCapacity = grainNum;
//            FGSyncArrayList<Integer> integers = new FGSyncArrayList<>(grainNum, addCapacity, new int[]{attemptsToFail, 0});
//            bwAdd.write("Initial capacity = " + addCapacity + " grainNum = " + grainNum + " attemptsToFail = " + attemptsToFail);
//            bwAdd.newLine();
//            JobCreator.addTest(integers, threads, bwAdd, 1_000_000, 100_000_000);
//            bwAdd.write(" Sub sizes = " + Arrays.toString(integers.eachSize()));
//            bwAdd.newLine();
//            bwAdd.write("-----------------------------------------------------");
//            bwAdd.newLine();
//            System.out.println("Size = " + integers.size() + " Sub sizes = " + Arrays.toString(integers.eachSize()));

            //удаление
            int rmvCapacity = 50_000_000;
            FGSyncArrayList<Integer> integers2 = new FGSyncArrayList<>(grainNum, rmvCapacity, new int[]{attemptsToFail, 0});
            bwRemove.write("Initial capacity = " + rmvCapacity + " grainNum = " + grainNum + " attemptsToFail = " + attemptsToFail);
            bwRemove.newLine();
            JobCreator.removeZerosTest(integers2, threads, bwRemove, rmvCapacity, 100_000, 2, true);
            bwRemove.write(" Sub sizes = " + Arrays.toString(integers2.eachSize()));
            bwRemove.newLine();
            bwRemove.write("-----------------------------------------------------");
            bwRemove.newLine();
            System.out.println("Size = " + integers2.size() + " Sub sizes = " + Arrays.toString(integers2.eachSize()));
        }


    }


    @Test
    public void testAdd() throws Exception {
        int numberOfThreads = 1; //ТЮНИТЬ
        int grainNum = 40;//ТЮНИТЬ
        int attemptsToFail = 200;//ТЮНИТЬ
        int elements = 150_000_000;

        FGSyncArrayList<Integer> integers = new FGSyncArrayList<>(grainNum, grainNum, new int[]{attemptsToFail, 0});
        List<Thread> threads = JobCreator.createAddThreads(integers, numberOfThreads, elements,100_000_000);

        long start = System.currentTimeMillis();
        threads.forEach(Thread::start);

        for (Thread thread : threads) {
            thread.join();
        }
        long result = System.currentTimeMillis() - start;
        System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
        System.out.println("Size = " + integers.size() + " Sub sizes = " + Arrays.toString(integers.eachSize()));
//        System.out.println(integers);
//        System.out.println(integers);
//        for (int i = 0; i < elements; i++) {
//            if(!integers.contains(i)){
//                System.out.println("missed "+i);
//            }
//        }

        assertEquals(elements, integers.size());
    }

    @Test
    public void testRemove() throws Exception {
        int numberOfThreads = 7;
        int elements = 100_000_000;
        int toRemove = 150_000;
        int grainNum = 40;//ТЮНИТЬ
        FGSyncArrayList<Integer> integers = new FGSyncArrayList<>(grainNum, elements, new int[]{3 * numberOfThreads, 0});
        JobCreator.fillList(integers, elements, 10);
        System.out.println("Size = " + integers.size() + " Sub sizes = " + Arrays.toString(integers.eachSize()));
//        System.out.println(integers);

        List<Thread> threads = JobCreator.createRemoveThreads(integers, numberOfThreads, toRemove);
        long start = System.currentTimeMillis();
        threads.forEach(Thread::start);
        for (Thread thread : threads) {
            thread.join();
        }
        long result = System.currentTimeMillis() - start;
        System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
        System.out.println("Size = " + integers.size() + " Sub sizes = " + Arrays.toString(integers.eachSize()));
//        System.out.println(integers);

        assertEquals(elements - toRemove, integers.size());
    }

    @Test
    public void AddMixRemoveTestsOutput() throws IOException, InterruptedException {
        File directory = new File("files\\mixedTests");
        directory.mkdirs();

        File output = new File("files\\mixedTests\\FGAddWithRemove.txt");
        //в данном тесте одниковое кол-во потоков удаляет и добавляет
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(output,true))) {
            int addThreads = 1;
            int removeThreads = 6;
            int addElements = 150_000_000;
            int removeElements = 10_000;
            int randomRange = 1000;
            int removeDelay = 10000;
            int grainNum = 10;
            SyncList<Integer> integers = new FGSyncArrayList<>(grainNum,grainNum);

            bw.write("Initial capacity and  grain = " + grainNum);
            bw.newLine();
            JobCreator.mixedTest(integers, addThreads, removeThreads, bw, addElements, removeElements, randomRange, removeDelay);
            bw.write("------------------------------------------");
            bw.newLine();

        }


    }

}