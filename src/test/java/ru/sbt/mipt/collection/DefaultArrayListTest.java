package ru.sbt.mipt.collection;

import org.junit.Test;
import ru.sbt.mipt.execution.ExecThread;
import ru.sbt.mipt.execution.JobCreator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jenius on 04.01.2016.
 */
public class DefaultArrayListTest {
    // тест на доавление одним потоком стандартного arrayList
    @Test
    public void addTest() throws IOException, InterruptedException {
        File directory = new File("files\\OneWayTests");
        directory.mkdirs();
        File outputAdd = new File("files\\OneWayTests\\defaultArrayAdd.txt");
        try (BufferedWriter bwAdd = new BufferedWriter(new FileWriter(outputAdd, true))) {
            int elements = 150_000_000;
            int randomRange = 100;
            int capacity = elements;

//            bwAdd.write("Тест на добавление одним потоком в стандартный ArrayList");
//            bwAdd.newLine();
//            bwAdd.write("Configuration: elements = " + elements + " random range = " + randomRange);
//            bwAdd.newLine();
//
//            bwAdd.write("Initial capacity = " + capacity);
//            bwAdd.newLine();

            List<Integer> integers = new ArrayList<>(capacity);
            Random rand = new Random(System.nanoTime());

            System.out.println("start add");
            long start = System.currentTimeMillis();
            for (int j = 0; j < elements; j++) {
                integers.add(randomRange + rand.nextInt(randomRange));
            }
            long result = System.currentTimeMillis() - start;
            System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
//            bwAdd.write("Executing time: " + (double) result / 1000.0 + " seconds");
//            bwAdd.newLine();
            System.out.println("Size " + integers.size());
//            bwAdd.write("added elements:  " + integers.size());
//            bwAdd.newLine();
//            bwAdd.write("-------------------------------------------");
//            bwAdd.newLine();
            assertEquals(elements, integers.size());


        }
    }

    @Test
    public void addTestDouble() throws IOException, InterruptedException {
        File directory = new File("files\\OneWayTests");
        directory.mkdirs();
        File outputAdd = new File("files\\OneWayTests\\defaultArrayAddGrain.txt");
        try (BufferedWriter bwAdd = new BufferedWriter(new FileWriter(outputAdd, true))) {
            int grainNum = 25;
            List<List<Integer>> grains = new ArrayList<>(grainNum);
            int elements = 200_000_000;
            int randomRange = 100_000_000;
            int eachCapacity = elements / grainNum+1;

            bwAdd.write("Тест на добавление одним потоком в " + grainNum + " стандартных ArrayList");
            bwAdd.newLine();
            bwAdd.write("Configuration: elements = " + elements + " random range = " + randomRange);
            bwAdd.newLine();
            bwAdd.write("Initial eachCapacity = " + 1);
            bwAdd.newLine();

            for (int i = 0; i < grainNum; i++) {
                grains.add(new ArrayList<>(1));
            }

            Random rand = new Random(System.nanoTime());
            System.out.println("start add");
            long start = System.currentTimeMillis();

            for (List<Integer> grain : grains) {
                for (int i = 0; i < eachCapacity; i++) {
                    grain.add(randomRange + rand.nextInt(randomRange));
                }
            }

            long result = System.currentTimeMillis() - start;
            System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
            bwAdd.write("Executing time: " + (double) result / 1000.0 + " seconds");
            bwAdd.newLine();
            int sum = 0;
            for (List<Integer> grain : grains) {
                sum += grain.size();
            }
            System.out.println("Total added " + sum);
            bwAdd.write("Total added " + sum);
            bwAdd.newLine();
            bwAdd.write("-------------------------------------------");
            bwAdd.newLine();
        }
    }

    @Test
    public void removeTest() throws IOException, InterruptedException {
        File directory = new File("files\\OneWayTests");
        directory.mkdirs();
        File output = new File("files\\OneWayTests\\defaultArrayRemove.txt");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(output, true))) {
            int elements = 4_000_000;
            int toRemove = 50_000;
            int randomRange = 10;
            int capacity = elements;
            bw.write("Тест на удаление одним потоком в стандартный ArrayList");
            bw.newLine();
            bw.write("Configuration: all elements = " + elements + " (Integers equal 0) to remove = " + toRemove + " random range = " + randomRange);
            bw.newLine();
            List<Integer> integers = new ArrayList<>(capacity);
            bw.write("Initial capacity = " + capacity);
            bw.newLine();
            Random rand = new Random(System.nanoTime());

            for (int j = 0; j < elements; j++) {
                integers.add(rand.nextInt(randomRange));
            }
            System.out.println("start remove");
            long start = System.currentTimeMillis();
            for (int j = 0; j < toRemove; j++) {
                integers.remove((Integer) 0);
            }
            long result = System.currentTimeMillis() - start;
            System.out.println("Executing time: " + (double) result / 1000.0 + " seconds");
            bw.write("Executing time: " + (double) result / 1000.0 + " seconds");
            bw.newLine();
            System.out.println("Size " + integers.size());
            bw.write("rest elements:  " + integers.size());
            bw.newLine();
            bw.write("-------------------------------------------");
            bw.newLine();
            assertEquals(elements - toRemove, integers.size());
        }
    }


}
